/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.flutter.plugins.inapppurchaseexample;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.qubiz.inAppPurchaseTest";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "0.0.3";
}
